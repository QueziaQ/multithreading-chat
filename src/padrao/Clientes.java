package padrao;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;

public class Clientes extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtMsg;
	private JTextField nick;
	static Socket socket;
	static DataInputStream entrada;
	static DataOutputStream saida;
	String nickN=null;
	String mens=null;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Clientes frame = new Clientes();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public Clientes() throws IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 627, 418);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtMsg = new JTextField();
		txtMsg.setEnabled(false);
		txtMsg.setBounds(55, 180, 430, 30);
		contentPane.add(txtMsg);
		txtMsg.setColumns(10);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setEnabled(false);
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String mens=txtMsg.getText();
				try {
					saida.writeUTF(mens);
					txtMsg.setText("");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnEnviar.setBounds(502, 180, 74, 30);
		contentPane.add(btnEnviar);
		
		JLabel lblChatGlobal = new JLabel("CHAT GLOBAL");
		lblChatGlobal.setFont(new Font("Comic Sans MS", Font.PLAIN, 19));
		lblChatGlobal.setForeground(Color.WHITE);
		lblChatGlobal.setBackground(Color.WHITE);
		lblChatGlobal.setBounds(230, 11, 145, 32);
		contentPane.add(lblChatGlobal);
		
		nick = new JTextField();
		nick.setBounds(153, 82, 127, 30);
		contentPane.add(nick);
		nick.setColumns(10);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (nick.getText().length()==0){
					JOptionPane.showMessageDialog(null,"Voc� precisa ter um NickName");
				}else{
					txtMsg.setEnabled(true);
					btnEnviar.setEnabled(true);
					nickN=nick.getText();
					nick.setEnabled(false);
					btnOk.setEnabled(false);
					try {
						saida.writeUTF(nickN);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
			}
		});
		btnOk.setBounds(290, 82, 89, 30);
		contentPane.add(btnOk);
		
		JLabel lblNickname = new JLabel("NickName\r\n\r\n");
		lblNickname.setForeground(Color.WHITE);
		lblNickname.setFont(new Font("Comic Sans MS", Font.PLAIN, 19));
		lblNickname.setBackground(Color.WHITE);
		lblNickname.setBounds(55, 80, 104, 32);
		contentPane.add(lblNickname);
		try {
			socket=new Socket("localhost",7777);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			entrada=new DataInputStream(socket.getInputStream());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			saida=new DataOutputStream(socket.getOutputStream());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Entrada ent=new Entrada(entrada);
		Thread thread=new Thread(ent);
		thread.start();
	}

class In implements Runnable{
	DataInputStream entrada;
	public In(DataInputStream entrada){
		this.entrada=entrada;
	}
	@Override
	public void run() {
		String texto;
		try {
			texto=entrada.readUTF();
			System.out.println(texto);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
}