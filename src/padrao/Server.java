package padrao;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class Server extends JFrame {
	/**
	 * 
	 */
	
	private JPanel contentPane;
	private static final long serialVersionUID = 1L;
	private static JTextArea txtC;
	static Usuarios[]usuario=new Usuarios[3];
	static ServerSocket servSocket;
	static  Socket socket;
	static DataOutputStream saida;
	static DataInputStream entrada;
	public Server() {
		setBackground(Color.DARK_GRAY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 609, 384);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtC = new JTextArea();
		txtC.setFont(new Font("Arial", Font.PLAIN, 16));
		txtC.setForeground(Color.WHITE);
		txtC.setBackground(Color.GRAY);
		txtC.setBounds(55, 81, 499, 223);
		contentPane.add(txtC);
		
		
		JLabel lblHistricoDeConexes = new JLabel("HIST\u00D3RICO DE CONEX\u00D5ES");
		lblHistricoDeConexes.setFont(new Font("Arial", Font.PLAIN, 24));
		lblHistricoDeConexes.setForeground(Color.WHITE);
		lblHistricoDeConexes.setBackground(Color.WHITE);
		lblHistricoDeConexes.setBounds(115, 11, 361, 67);
		contentPane.add(lblHistricoDeConexes);
		
		new Thread( () -> {
			txtC.append("Aguarde .....Conectando ao server.....\n");
			try {
				servSocket=new ServerSocket(7777);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			txtC.append("Servidor Iniciado!!!!");
			while(true){
			try {
				socket=servSocket.accept();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(int i=0;i<3;i++){
				txtC.append("Conex�o:"+socket.getInetAddress()+"\n");
				try {
					saida=new DataOutputStream(socket.getOutputStream());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					entrada=new DataInputStream(socket.getInputStream());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(usuario[i]==null){
					usuario[i]=new Usuarios(saida,entrada,usuario);
					Thread thread=new Thread(usuario[i]);
					thread.start();
					break;
				}
			}
			
			}
		}).start();
	}
	public static void main(String[]args) throws Exception{
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Server frame = new Server();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	class Usuarios implements Runnable{
		DataOutputStream saida; 
		DataInputStream entrada;
		String nick;
		Usuarios[]usuario=new Usuarios[3];
			public Usuarios(DataOutputStream saida, DataInputStream entrada,Usuarios[]usuario){
				this.entrada=entrada;
				this.saida=saida;
				this.usuario=usuario;
			}

		@Override
		public void run() {
			try {
				nick=entrada.readUTF();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			while(true){
				try {
					String texto=entrada.readUTF();
					for(int i=0;i<3;i++){
						if(usuario[i]!=null){
							usuario[i].saida.writeUTF(" "+ nick +" "+texto);
						}
					}
					txtC.append(""+nick +": "+texto+"\n");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					this.saida=null;
					this.entrada=null;
				}
			}
			
		}
		
	}
}
